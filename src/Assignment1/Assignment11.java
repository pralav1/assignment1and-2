package Assignment1;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.edge.EdgeDriver;


public class Assignment11 {
	public void kill(String s_drname) throws IOException {
    if(s_drname.equals("gecko"))
        Runtime.getRuntime().exec("taskkill /f /im geckodriver.exe");
    if(s_drname.equals("chrome"))
        Runtime.getRuntime().exec("taskkill /f /im chromedriver.exe");
    if(s_drname.equals("headless"))
        Runtime.getRuntime().exec("taskkill /f /im chromedriver.exe");
    if(s_drname.equals("ie"))
        Runtime.getRuntime().exec("taskkill /f /im IEDriverServer.exe");
    if(s_drname.equals("edge"))
        Runtime.getRuntime().exec("taskkill /f /im MicrosoftWebDriver.exe");
}


public static void main(String args[]) throws IOException
{
	System.setProperty("webdriver.gecko.driver","D:\\ALM image\\Sel\\geckodriver-v0.23.0-win64\\geckodriver.exe");
	WebDriver driver = new FirefoxDriver();
	driver.get("http://www.store.demoqa.com");
	
	
	System.setProperty("webdriver.chrome.driver","D:\\ALM image\\Sel\\chromedriver.exe");
	WebDriver dr=new ChromeDriver();
	dr.get("http://www.store.demoqa.com");
	
	
	System.setProperty("webdriver.ie.driver", "D:\\ALM image\\Sel\\IEDriverServer_Win32_2.45.0\\IEDriverServer.exe");
	WebDriver drr=new InternetExplorerDriver();
	drr.get("http://www.store.demoqa.com");
	
	
	System.setProperty("webdriver.edge.driver","D:\\ALM image\\Sel\\MicrosoftWebDriver.exe");
	WebDriver dr2=new EdgeDriver();
	dr2.get("http://www.store.demoqa.com");
	driver.close();
	
    System.setProperty("webdriver.chrome.driver","D:\\ALM image\\Sel\\chromedriver.exe");
    ChromeOptions options = new ChromeOptions();
    options.addArguments("headless");
    WebDriver driver = new ChromeDriver(options);
    driver.get("https://contentstack.built.io");
    
	
	Assignment11 assessment=new Assignment11();
    assessment.kill("gecko");
    assessment.kill("chrome");
    assessment.kill("headless");
    assessment.kill("ie");
    assessment.kill("edge");

}

}
