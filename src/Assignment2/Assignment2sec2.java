package Assignment2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;



public class Assignment2sec2 {
	static WebDriver dr;
	
	@Rule public TestName name = new TestName();
	 
	@BeforeClass
	public static void start1()
	{
		System.setProperty("webdriver.chrome.driver","D:\\ALM image\\Sel\\chromedriver.exe");
		dr=new ChromeDriver();
		dr.get("http://shop.demoqa.com/");
		dr.manage().window().maximize();
	}

	@Test
	public void test1()
	{	
		String url=dr.getCurrentUrl();
		System.out.println(url);
		assertEquals("http://shop.demoqa.com/",url);
	}
	
	@Test
	public void test2()
	{	
		String ti=dr.getTitle();
		System.out.println(ti);
		assertNotNull(ti);
	}

	@After
	public void close()
	{
		System.out.println("Test "+name.getMethodName()+" completed");
	}
	
	@AfterClass
	public static void close1()
	{
		dr.close();
	}

}
