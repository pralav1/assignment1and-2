package Assignment2;

import org.openqa.selenium.WebDriver;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

public class Assignment2sec1 {
	
	WebDriver dr;
	@Before
	public void start()
	{
		System.setProperty("webdriver.chrome.driver","D:\\ALM image\\Sel\\chromedriver.exe");
		
	}
	
	@Test
	public void test1()
	{	
		dr=new ChromeDriver();
		dr.get("http://shop.demoqa.com/");
		String url=dr.getCurrentUrl();
		System.out.println(url);
		assertEquals("http://shop.demoqa.com/",url);
		
	}
	
	@Test
	public void test2()
	{	
		dr=new ChromeDriver();
		dr.get("http://shop.demoqa.com");
		String ti=dr.getTitle();
		System.out.println(ti);
		assertNotNull(ti);
	}
	
	
	

	@After
	public void close()
	{
		dr.close();
	}

}

