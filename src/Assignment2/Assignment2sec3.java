package Assignment2;
import static org.junit.Assert.assertEquals;
import java.util.Arrays;
import java.util.Collection;
import org.junit.*;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

 @RunWith(Parameterized.class)
public class Assignment2sec3 {
WebDriver dr;
	
	@Parameterized.Parameters()

	public static Collection<Object[]> data(){

		Object[][] data = new Object[][] {

			{"ie"},{"chrome"},{"edge"},{"firefox"}

		};

		return Arrays.asList(data);

	}

	

	@Parameterized.Parameter(0)
	public String s;

	@Before
	public void Before() {

		switch(s) {

		case "ie":

			System.setProperty("webdriver.ie.driver", "D:\\ALM image\\Sel\\IEDriverServer.exe");

			dr = new InternetExplorerDriver();

			break;

		case "chrome" :

			System.setProperty("webdriver.chrome.driver", "D:\\ALM image\\Sel\\chromedriver.exe");

			dr = new ChromeDriver();

			break;

		case "edge":

			System.setProperty("webdriver.edge.driver","D:\\ALM image\\Sel\\MicrosoftWebDriver.exe");

			dr = new EdgeDriver();

			break;

		case "firefox":

			System.setProperty("webdriver.gecko.driver", "D:\\ALM image\\Sel\\geckodriver.exe");

			dr = new FirefoxDriver();

		}

		dr.get("http://shop.demoqa.com/");
		dr.manage().window().maximize();

	}

	

	@Test
	public void test1() throws InterruptedException {

		Thread.sleep(200);

		assertEquals("http://shop.demoqa.com/",dr.getCurrentUrl());
	
	}

	@Test
	public void test2()  {
		
		assertEquals("Shoptools",dr.getTitle());
		String s1=dr.getTitle();
		System.out.println(s1);
		
	}

	@After
	public void closeApp() {

		dr.close();
	}

 }
